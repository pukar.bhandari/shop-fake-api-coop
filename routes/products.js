const express = require('express');

const {
    getProducts,
    getProduct,
    updateProduct,
    createProduct,
    deleteProduct
} = require('../controllers/products');

const Product = require('../models/Product');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');

router.
route('/').
get(advancedResults(Product), getProducts).
put(updateProduct).
post(createProduct);

router.route('/:id')
    .get(getProduct)
    .delete(deleteProduct);

module.exports = router;
