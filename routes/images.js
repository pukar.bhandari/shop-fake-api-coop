const express = require('express');

const { photoUpload } = require('../controllers/images');

const router = express.Router();

router.
route('/').
post(photoUpload);

module.exports = router;