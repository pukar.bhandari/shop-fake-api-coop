const express = require('express');
const {getCustomers, getCustomer, createCustomer, deleteCustomer} = require('../controllers/customers');

const Customer = require('../models/Customer');

// Include other resource routers
const cartRouter = require('./cart');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');

//  Re-route into other resource routers
router.use('/:customerId/cart', cartRouter);

router.route('/').
get(advancedResults(Customer, 'cart'),getCustomers).
post(createCustomer);

router.route('/:id')
    .get(getCustomer)
    .delete(deleteCustomer);

module.exports = router;