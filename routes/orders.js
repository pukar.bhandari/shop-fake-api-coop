const express = require('express');

const {
    getOrders,
    getOrder,
    createOrder,
    updateOrder,
    deleteOrder
} = require('../controllers/orders');

const Order = require('../models/Order');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');

router.
route('/').
get(advancedResults(Order), getOrders).
put(updateOrder).
post(createOrder);

router.route('/:id')
    .get(getOrder)
    .delete(deleteOrder);

module.exports = router;
