const express = require('express');

const {
    getSmses,
    getSms,
    createSms,
    updateSms,
    deleteSms
} = require('../controllers/sms');

const Sms = require('../models/Sms');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');

router.
route('/').
get(advancedResults(Sms), getSmses).
put(updateSms).
post(createSms);

router.route('/:id')
    .get(getSms)
    .delete(deleteSms);

module.exports = router;
