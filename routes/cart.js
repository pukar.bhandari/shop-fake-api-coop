const express = require('express');

const {
    getCartItems,
    getCartItem,
    addCartItem,
    deleteCartItem,
    updateCartItem
} = require('../controllers/cart');

const Cart = require('../models/Cart');

const router = express.Router({mergeParams: true});

const advancedResults = require('../middleware/advancedResults');

router.route('/').get(advancedResults(Cart, {
    path: 'customer',
    select: 'name'
}), getCartItems).post(addCartItem);

router.route('/:id').get(getCartItem)
    .put(updateCartItem)
    .delete(deleteCartItem);

module.exports = router;