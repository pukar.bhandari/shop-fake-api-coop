const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const errorHandler = require('./middleware/error');
const connectDB = require('./config/db');

// Load env vars
dotenv.config({path: './config/config.env'});

//Connect to database
connectDB().then();

// route files
const products = require('./routes/products');
const images = require('./routes/images');
const orders = require('./routes/orders');
const sms = require('./routes/sms');
const customer = require('./routes/customer');
const cart = require('./routes/cart');

const app = express();

// Body parser
app.use(express.json());

app.use(express.urlencoded({
    extended: true
}))

// Dev logging middleware
if(process.env.NODE_ENV === "development") {
    app.use(morgan('dev'));
}

//File uploading
app.use(fileUpload());

// Enable CORS
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Mount routers
app.use('/api/v1/products', products);
app.use('/api/v1/images', images);
app.use('/api/v1/orders', orders);
app.use('/api/v1/sms', sms);
app.use('/api/v1/customer', customer);
app.use('/api/v1/cart', cart);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
    PORT,
    console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold)
);

// Handle unhandled promise rejections
process.on('unhandledRejection', (err,promise) => {
    console.log(`Error: ${err.message}`.red);
    // Close server & exit process
    server.close(() => process.exit(1));
})

