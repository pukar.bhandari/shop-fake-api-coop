const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');

// Load env vars
dotenv.config({path: './config/config.env'});

// Load models
const Product = require('./models/Product');
const Order = require('./models/Order');
const Sms = require('./models/Sms');
const Cart = require('./models/Cart');
const Customer = require('./models/Customer');

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Read JSON files
const products = JSON.parse(fs.readFileSync(`${__dirname}/_data/products.json`, 'utf-8'));
const orders = JSON.parse(fs.readFileSync(`${__dirname}/_data/orders.json`, 'utf-8'));
const customers = JSON.parse(fs.readFileSync(`${__dirname}/_data/customers.json`, 'utf-8'));
const cart = JSON.parse(fs.readFileSync(`${__dirname}/_data/cart.json`, 'utf-8'));

// Import into DB
const importData = async () => {
    try{
        // await Product.create(products);
        // await Order.create(orders);
        await Customer.create(customers);
        await Cart.create(cart);
        console.log('Data imported...'.green.inverse);
        process.exit();
    } catch(err) {
        console.error(err);
    }
}

// Delete data
const deleteData = async () => {
    try{
        await Product.deleteMany();
        await Order.deleteMany();
        await Sms.deleteMany();
        await Cart.deleteMany();
        await Customer.deleteMany();
        console.log('Data destroyed...'.red.inverse);
        process.exit();
    } catch(err) {
        console.error(err);
    }
}

if(process.argv[2] === '-i') { // node seeder -i
    importData();
} else if(process.argv[2] === '-d') { // node seeder -d
    deleteData();
}
