const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Product = require('../models/Product');

// @desc        Get all products
// @route       GET /api/v1/products
// @access      Public
exports.getProducts = asyncHandler(async (req, res, next) => {
    res.status(200)
        .json(res.advancedResults);
});

// @desc        Get single product
// @route       GET /api/v1/products/:id
// @access      Public
exports.getProduct = asyncHandler(async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    if (!product) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    }

    res.status(200).json({
        success: true,
        data: product
    })
});

// @desc        Create new product
// @route       POST /api/v1/products
// @access      Private
exports.createProduct = asyncHandler(async (req, res, next) => {

    const product = await Product.create(req.body);

    res.status(201).json({
        success: true,
        data: product
    });
});

// @desc        Update product
// @route       PUT /api/v1/products
// @access      Private
exports.updateProduct = asyncHandler(async (req, res, next) => {
    let product = await Product.findById(req.body._id);

    if (!product) {
        return next(new ErrorResponse(`Product not found with id of ${req.body._id}`, 404));
    }

    product = await Product.findByIdAndUpdate(req.body._id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({success: true, data: product});
});

// @desc        Delete product
// @route       DELETE /api/v1/products/:id
// @access      Private
exports.deleteProduct = asyncHandler(async (req, res, next) => {
    const product = await Product.findById(req.params.id);

    if (!product) {
        return next(new ErrorResponse(`Product not found with id of ${req.params.id}`, 404));
    }

    product.remove();

    res.status(200).json({success: true, data: {}});
});


