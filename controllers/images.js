const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Image = require('../models/Image');


// @desc        Upload photo
// @route       POST /api/v1/images
// @access      Private
exports.photoUpload = asyncHandler(async (req, res, next) => {

    if (!req.files) {
        return next(new ErrorResponse('Please upload a file', 400));
    }

    const file = req.files.file;

    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse('Please upload an image file', 400));
    }

    // Check filesize
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400));
    }

    // Create custom filename
    const randomFileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    file.name = `photo_${randomFileName}${path.parse(file.name).ext}`;

    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }

        await Image.create({
            largeImage: file.name,
            thumbnail: file.name
        });

        res.status(200).json({
            success: true,
            data: {
                largeImage: 'http://localhost:5000/uploads/' + file.name,
                thumbnail: 'http://localhost:5000/uploads/' + file.name
            }
        });
    })

});