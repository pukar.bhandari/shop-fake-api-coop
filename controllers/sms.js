const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Sms = require('../models/Sms');

// @desc        Get all sms
// @route       GET /api/v1/sms
// @access      Public
exports.getSmses = asyncHandler(async (req, res, next) => {
    res.status(200)
        .json(res.advancedResults);
});

// @desc        Get single sms
// @route       GET /api/v1/smss/:id
// @access      Public
exports.getSms = asyncHandler(async (req, res, next) => {
    const sms = await Sms.findById(req.params.id);
    if (!sms) {
        return next(new ErrorResponse(`Sms not found with id of ${req.params.id}`, 404));
    }

    res.status(200).json({
        success: true,
        data: sms
    })
});

// @desc        Create new sms
// @route       POST /api/v1/sms
// @access      Private
exports.createSms = asyncHandler(async (req, res, next) => {

    const sms = await Sms.create(req.body);

    res.status(201).json({
        success: true,
        data: sms
    });
});

// @desc        Update sms
// @route       PUT /api/v1/sms
// @access      Private
exports.updateSms = asyncHandler(async (req, res, next) => {
    let sms = await Sms.findById(req.body._id);

    if (!sms) {
        return next(new ErrorResponse(`Sms not found with id of ${req.body._id}`, 404));
    }

    sms = await Sms.findByIdAndUpdate(req.body._id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({success: true, data: sms});
});

// @desc        Delete sms
// @route       DELETE /api/v1/sms/:id
// @access      Private
exports.deleteSms = asyncHandler(async (req, res, next) => {
    const sms = await Sms.findById(req.params.id);

    if (!sms) {
        return next(new ErrorResponse(`Sms not found with id of ${req.params.id}`, 404));
    }

    sms.remove();

    res.status(200).json({success: true, data: {}});
});
