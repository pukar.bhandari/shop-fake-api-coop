const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Cart = require('../models/Cart');
const Customer = require('../models/Customer');

// @desc        Get cart items
// @route       GET /api/v1/cart
// @route       GET /api/v1/customer/:customerId/cart
// @access      Public
exports.getCartItems = asyncHandler(async (req, res, next) => {
    if (req.params.customerId) {
        const cart =  await Cart.find({customer: req.params.customerId});

        return res.status(200).json({
            success: true,
            count: cart.length,
            data: cart
        })
    } else {
        res.status(200).json(res.advancedResults);
    }
});

// @desc        Get a single cart item
// @route       GET /api/v1/cart/:id
// @access      Public
exports.getCartItem = asyncHandler(async (req, res, next) => {
    const cart = await Cart.findById(req.params.id).populate({
        path: 'customer',
        select: 'name'
    }).populate({
        path: 'product'
    });
    if (!cart) {
        return next(new ErrorResponse(`No cart item with the id of ${req.params.id}`), 404)
    }
    res.status(200).json({success: true, data: cart});
});

// @desc        Add course
// @route       POST /api/v1/customer/:customerId/cart
// @access      Private
exports.addCartItem = asyncHandler(async (req, res, next) => {
    req.body.customer = req.params.customerId;

    const customer = await Customer.findById(req.params.customerId);

    if (!customer) {
        return next(new ErrorResponse(`No customer with the id of ${req.params.customerId}`), 404)
    }

    const cart = await Cart.create(req.body);

    res.status(200).json({success: true, data: cart});
})

// @desc        Update cart
// @route       PUT /api/v1/cart/:id
// @access      Private
exports.updateCartItem = asyncHandler(async (req, res, next) => {

    let cart = await Cart.findById(req.params.id);

    if (!cart) {
        return next(new ErrorResponse(`No cart item with the id of ${req.params.id}`), 404)
    }


    cart = await Cart.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({success: true, data: cart});
})

// @desc        Delete items in cart
// @route       DELETE /api/v1/cart/:id
// @access      Private
exports.deleteCartItem = asyncHandler(async (req, res, next) => {
    const cart = await Cart.findById(req.params.id);

    if (!cart) {
        return next(new ErrorResponse(`No cart item with the id of ${req.params.id}`), 404)
    }

    await cart.remove();

    res.status(200).json({success: true, data: {}});
})