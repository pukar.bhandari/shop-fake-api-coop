const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Order = require('../models/Order');

// @desc        Get all orders
// @route       GET /api/v1/orders
// @access      Public
exports.getOrders = asyncHandler(async (req, res, next) => {
    res.status(200)
        .json(res.advancedResults);
});

// @desc        Get single order
// @route       GET /api/v1/orders/:id
// @access      Public
exports.getOrder = asyncHandler(async (req, res, next) => {
    const order = await Order.findById(req.params.id);
    if (!order) {
        return next(new ErrorResponse(`Order not found with id of ${req.params.id}`, 404));
    }

    res.status(200).json({
        success: true,
        data: order
    })
});

// @desc        Create new order
// @route       POST /api/v1/orders
// @access      Private
exports.createOrder = asyncHandler(async (req, res, next) => {

    const order = await Order.create(req.body);

    res.status(201).json({
        success: true,
        data: order
    });
});

// @desc        Update order
// @route       PUT /api/v1/orders
// @access      Private
exports.updateOrder = asyncHandler(async (req, res, next) => {
    let order = await Order.findById(req.body._id);

    if (!order) {
        return next(new ErrorResponse(`Order not found with id of ${req.body._id}`, 404));
    }

    order = await Order.findByIdAndUpdate(req.body._id, req.body, {
        new: true,
        runValidators: true
    });
    res.status(200).json({success: true, data: order});
});

// @desc        Delete order
// @route       DELETE /api/v1/orders/:id
// @access      Private
exports.deleteOrder = asyncHandler(async (req, res, next) => {
    const order = await Order.findById(req.params.id);

    if (!order) {
        return next(new ErrorResponse(`Order not found with id of ${req.params.id}`, 404));
    }

    order.remove();

    res.status(200).json({success: true, data: {}});
});
