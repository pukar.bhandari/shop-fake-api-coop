const mongoose = require('mongoose');

const SmsSchema = new mongoose.Schema({
    thankYouMessage: String,
    newMessage: {
        comment: String
    },
    productionMessage: {
        comment: String
    },
    clearMessage: {
        comment: String
    }
});

module.exports = mongoose.model('Sms', SmsSchema);
