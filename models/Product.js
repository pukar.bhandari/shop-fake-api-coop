const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true,'Please add a title'],
        trim: true,
        maxlength: [50, 'Title cannot be more than 50 characters']
    },
    category: {
        type: String,
        enum: [
            'Delikatessen',
            'Bageren',
            'Slagter',
            'Fisk',
            'Frugt',
            'Grønt'
        ]
    },
    description: {
        type: String,
        required: [true,'Please add a description'],
        trim: true,
        maxlength: [100, 'Title cannot be more than 100 characters']
    },
    largeImage: {
        // Array of strings
        type: [String],
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    totalOffer: {
        type: Number,
        required: true
    },
    offerPerDay: {
        type: Number,
        required: true
    },
    ended_description: {
        type: String,
        required: true,
        trim: true
    },
    productionTime: {
        type: Number,
        required: true
    },
    productActive: {
        type: Boolean,
        default: false
    },
    allowComments: {
        type: Boolean,
        default: false
    },
    deposit: {
        type: Boolean,
        default: false
    },
    alcoholic: {
        type: Boolean,
        default: false
    },
    pickUpPeriod: {
        type: Boolean,
        default: false
    },
    pickUp: {
        StartDate: Date,
        EndDate: Date,
    },
    activeDays: String
});

ProductSchema.pre('save', function (next) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate = new Date(this.pickUp.StartDate);
    const secondDate = new Date(this.pickUp.EndDate);
    this.activeDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
    next();
});

module.exports = mongoose.model('Product', ProductSchema);
