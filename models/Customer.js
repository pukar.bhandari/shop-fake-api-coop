const mongoose = require('mongoose');

const CustomerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a name'],
        maxlength: [50, 'Name cannot be more than 50 characters']
    }
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

// Reverse populate with virtuals
CustomerSchema.virtual('cart', {
    ref: 'Cart',
    localField: '_id',
    foreignField: 'customer',
    justOne: false
})

module.exports = mongoose.model('Customer', CustomerSchema);