const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema({
    orderNumber: {
        type: Number,
        default: function () {
            return Math.floor(1000 + Math.random() * 9000)
        },
        immutable: true
    },
    category: {
        type: String,
        enum: [
            'Delikatessen',
            'Bageren',
            'Slagter',
            'Fisk',
            'Frugt',
            'Grønt'
        ]
    },
    bookingDate: {
        type: Date,
        required: true
    },
    pickupDate: {
        type: Date,
        required: true
    },
    phoneNumber: {
        type: Number,
        require: true
    },
    totalPrice: String,
    totalQuantity: Number,
    orderStatus: {
        type: String,
        default: '1',
        enum: [
            '1',
            '2',
            '0',
            '-1',
            '3'
        ]
    },
    deliveryStatus: {
        type: String,
        enum: [
            '1',
            '2',
            '0',
            '-1'
        ]
    },
    priority: {
      type: String,
      default: 'normal',
      enum: [
          'urgent',
          'important',
          'normal'
      ]
    },
    approvedDate: {
        type: Date
    },
    orders: {
        type: [Object], //{ productTitle: String, quantity: Number,price: Number }
        required: true
    },
    repay: Boolean,
    notificationMessage: [Object],
    orderComment: String,
    orderMessage: String,
    sentComment: [Object],
    pickedUpComment: Object// { comment: string; sent_date: Date }
});

module.exports = mongoose.model('Order', OrderSchema);
