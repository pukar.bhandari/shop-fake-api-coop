const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
    largeImage: String,
    thumbnail: String
});

module.exports = mongoose.model('Image', ImageSchema);