const mongoose = require('mongoose');

const CartSchema = new mongoose.Schema({
    product: {
        type: mongoose.Schema.ObjectId,
        ref: 'Product',
        required: true
    },
   shopId: {
        type: Number,
        required: [true, 'Shop Id is required']
    },
    comment: {
        type: String
    },
    quantity: {
        type: Number,
        required: [true, 'Quantity is required']
    },
    customer: {
        type: mongoose.Schema.ObjectId,
        ref: 'Customer',
        required: true
    }
})

module.exports = mongoose.model('Cart', CartSchema);