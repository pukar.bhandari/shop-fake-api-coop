// Opret nyt product page - Slide 17
// @desc        Add new product
// @route       POST save-product.php
// &
// Edit produkt page - Slide 18
// @desc         Get product
// @route        GET get-single-product.php
export interface Product{
    id: number;
    largeImage: Array<string>[];
    category: string; // Delikatessen/ Bageren/ Slagter/ Fisk/ Frugt/ Grønt
    title: string;
    description: string;
    price: number;
    totalOffer: number;
    offerPerDay: number;
    ended_description: string;
    productionTime: number;
    productActive: boolean;
    allowComments: boolean;
    deposit: number;
    pickupPeriod: boolean;
    start_date: Date;
    end_date: Date;
}

// Produktkatalog page - Slide 20
// @desc         Get all products
// @route        GET get-products.php
export interface Products {
    id: number;
    largeImage: string;
    category: string;
    title: string;
    totalOffer: number;
    activeDays: string;
    start_date: Date;
    end_date: Date;
}

// Home page - Slide 1
// @desc        Get new orders
// @route       GET get-orders.php
// @params	    Filter - All/ Delikatessen/ Bageren/ Slagter/ Fisk/ Frugt/ Grønt
// @params      new
// @params      Start Date and End Date
export interface NewOrders {
    id: number;
    orderNumber: number;
    category: string;
    bookingDate: Date;

}

// Bestillinger page - Slide 21, Slide 23, Slide 25 / Afsluttet ordre page - Slide 5
// @desc         Get all orders
// @route        GET get-orders.php
// @params	     Filter - All/ Delikatessen/ Bageren/ Slagter/ Fisk/ Frugt/ Grønt
export interface Orders {
    id: number;
    orderNumber: number;
    category: string;
    bookingDate: Date;
    pickupDate: Date;
    totalPrice: number;
    totalQuantity: number;
    orderStatus: string; //Ny (new) - 1/ I production (inProduction) - 2/ klargør (prepared)- 0/ klar (clear) - -1/ annuller (cancel) - 3
    deliveryStatus: string; // Ikke afhentet (Not picked up)- 1 /Afhentet (Picked up) - 2/ Afvist (Rejected)- 0/ Refunderet (refunded) - -1
}

// Ny/ I production/ klargør/ klar Bestillinger page - Slide 22, Slide 24, Slide 30 / Afsluttet bestilling page - Slide 6
// @desc        Get order
// @route       GET get-single-order.php
export interface Order {
    id: number;
    orderNumber: number;
    category: string;
    bookingDate: Date;
    pickupDate: Date;
    approvedDate: Date;
    phoneNumber: number;
    orders: OrderList[];
    orderComment: string;
    orderMessage: string;
    sentComment: Message[];
    orderStatus: string; //Ny (new) - 1/ I production (inProduction) - 2/ klargør (prepared)- 0/ klar (clear) - -1
    deliveryStatus: string; // Ikke afhentet (Not picked up)- 1 /Afhentet (Picked up) - 2/ Afvist (Rejected)- 0/ Refunderet (refunded) - -1
}

export interface OrderList {
    productTitle: string;
    quantity: number;
    price: number;
}

export interface Message {
    comment: string;
    sent_date: Date
}

// SMS indstillinger page - Slide 8
// @desc         Get SMS
// @route        GET get-client-sms.php
// SMS indstillinger page - Slide 8, 9, 10, 11
// @desc         Save SMS / Update SMS
// @route        POST save-client-sms.php / PUT save-client-sms.php
export interface SMSText {
    newMessage: string;
    inProductionMessage: string;
    prepareMessage: string;
    clearMessage: string;
    endMessage: string;
    reminder: ReminderList[];
}

export interface ReminderList {
    reminderMessage: string;
    timeAfterMessage: number;
}
