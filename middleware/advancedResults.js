const advancedResults = (model, populate) => async (req,res,next) => {
    let query;

    //  Copy req.query
    const reqQuery = { ...req.query };

    // Fields to exclude
    const removeFields = ['category','orderStatus','finished'];

    //  Loop over removeFields and delete them from reqQuery
    removeFields.forEach(param => delete reqQuery[param]);

    // Create query string
    let queryStr = JSON.stringify(reqQuery);

    //Finding resource
    query = model.find(JSON.parse(queryStr));

    // Select Fields
    if(req.query.category) {
        query = query.find({category: req.query.category});
    }

    if(req.query.orderStatus) {
        query = query.find({orderStatus: req.query.orderStatus});
        query = query.find({deliveryStatus: null})
    }

    if(req.query.finished) {
        query = query.find({deliveryStatus: {$exists: true}})
    }

    if(populate) {
        query = query.populate(populate);
    }

    // Executing query
    const results = await query;

    res.advancedResults = {
        success: true,
        count: results.length,
        data: results
    }

    next();
}

module.exports = advancedResults;
